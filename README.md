# ResourceParser 🚀

This tool is used to generate resource definitions from source code files.

## Usage 🏃
Go to the published folder or wherever you put the actual `ResourceParser.exe` file. Then arguments are as described below:

1. Output folder - Folder into which tool will generate resource definitions. Files will be put there and they will be called same as ones processed (with appended extension .resources). For example if you process `Something.html` in output folder you shall get `Something.resources`
2. Files to process - Actual files with code which shall be processed in order to get resource definitions.

Sample usage would then look something like next call:
`ResourceParser.exe C:/Users/User/Desktop C:/Project/Source/Source.ascx`

## Supported file types 📁
In the code there is an interface named `IEntrySourceSink` which is base for getting resource string from different sources. Currently there is only one implemented (called `HtmlEntrySourceSink`) which supports .html, .ascx, .aspx, .cshtml, .cs (only MVC models currently), .js/ts (be careful with picked results) files. In order to support other types create a new implementation of the mentioned interface in console app and it should get picked up automatically in the code (also if you'll try to parse file with new extension that you supported your newly created implementation shall be used)

## Interactive mode ⌨
Currently .cshtml file sink and .ascx, .aspx file sink (`CshtmlEntrySourceSink`, `HtmlEntrySourceSink`) have a simple interactive mode support in it. While scanning through content application will stop and ask you to fill up (or skip) details for specific strings (ie. detected razor expressions, for example `@Html.*` or webforms expression like `<%= this.Calculate() %>`). What you actually insert there shall be used as base text for that resource entry

## Build/Publish 🔨

There is a script at the root of the repo that can publish actual console tool. Mono linker is used while publishing in order to reduce the size of 
built app (self-contained app). Script is called [publish.ps1](https://bitbucket.org/dpavlovic2/resourceparser/src/2c46034187a0fa4a2c367d323a4951d7d7dc3036/publish.ps1?at=master&fileviewer=file-view-default). Also if you don't want to build it yourself you can find latest builds in `./Builds` folder