﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AngleSharp.Extensions;
using ResourceParser.Common;

namespace ResourceParser
{
    class Program
    {
        //tested with EditEmailTemplates.ascx;
        //tested with apcheckdetails.html
        //tested with ManageExports.aspx
        //tested with Address -> Detail.cshtml
        //tested with Requirement -> RequirementModel.cs
        //tested with HistoryLog.ascx

        //create instance when getting sink (register funcs which create new instances)
        static List<IEntrySourceSink> AvailableSinks = Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(typeof(IEntrySourceSink).IsAssignableFrom)
            .Select(Activator.CreateInstance)
            .Select(instance => instance as IEntrySourceSink)
            .ToList();

        static void Main(string[] args)
        {
            if (args.Length < 2)
                throw new ArgumentException("You need to supply atleast an output folder and single file to cover");

            Task.WhenAll(args.Skip(1).ToList().Select(filePath => CreateExportResourceTask(args[0], filePath))).Wait();
        }

        private static async Task CreateExportResourceTask(string outputPath, string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            var output = Path.Combine(outputPath, $"./{fileInfo.Name}.resources");
            var wantedSink = AvailableSinks.FirstOrDefault(x => x.Extensions.Contains(fileInfo.Extension))
                ?? throw new NullReferenceException($"Sink for {fileInfo.Extension} is not implemented");

            var className = fileInfo.Name.Split('.').First();
            var text = wantedSink.GetValues(fileInfo);
            var entries = GenerateResourceEntries(text, className);

            using (var stream = new FileStream(output, FileMode.Create))
            using (var writer = new StreamWriter(stream))
                foreach (var entry in entries)
                    await writer.WriteLineAsync(entry);

            var outputAdditional = Path.Combine(outputPath, $"./{fileInfo.Name}.setup.resources");
            var additionalContent = GenerateAdditionalContent(text, className);

            using (var stream = new FileStream(outputAdditional, FileMode.Create))
            using (var writer = new StreamWriter(stream))
                foreach (var entry in additionalContent)
                    await writer.WriteLineAsync(entry);
        }

        private static List<string> GenerateResourceEntries(IEnumerable<string> texts, string fileName)
        {
            var entries = new List<string>();

            foreach (var text in texts)
            {
                var name = UpperCase(text);
                entries.Add(string.Format(Constants.ResEntryFormat, name, text, fileName));
            }

            return entries;
        }

        private static string UpperCase(string text)
        {
            var joined = string.Join(string.Empty, text.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x =>
                    {
                        var characters = x.ToCharArray();
                        characters[0] = char.ToUpper(characters[0]);

                        return string.Join(string.Empty, characters);
                    }));

            var name = string.Join(string.Empty, joined.Where(char.IsLetter));

            return name;
        }

        private static List<string> GenerateAdditionalContent(IEnumerable<string> texts, string className)
        {
            var additionalDefs = new List<string>();

            void GenerateCodeBehindDefs(IList<string> definitions)
            {
                definitions.Add($"public struct Labels{Environment.NewLine}{{");
                texts.ToList().ForEach(x => definitions.Add($"\t{string.Format(Constants.WebFormsCodeBehind, UpperCase(x))}"));
                definitions.Add("}");

                definitions.Add(Environment.NewLine);

                definitions.Add("protected Labels Labels;");
                definitions.Add($"public void CreateLabels(){Environment.NewLine}{{");
                definitions.Add($"\tLabels = new Labels{Environment.NewLine}\t{{");
                texts.ToList().ForEach(x => definitions.Add($"\t\t{string.Format("{0} = LocalizationService.Localize(lr{1}.{0})", UpperCase(x), className)},"));
                definitions.Add("\t}");
                definitions.Add("}");
            }

            void GenerateRazorDefs(IList<string> definitions)
                => texts.ToList().ForEach(x => definitions.Add(string.Format(Constants.RazorExpression, $"lr{className}", UpperCase(x))));

            GenerateCodeBehindDefs(additionalDefs);
            additionalDefs.Add(Environment.NewLine);
            GenerateRazorDefs(additionalDefs);

            return additionalDefs;
        }
    }
}
