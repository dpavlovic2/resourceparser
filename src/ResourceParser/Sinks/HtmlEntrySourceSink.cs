﻿using AngleSharp.Dom;
using AngleSharp.Parser.Html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ResourceParser.Common
{
    public class HtmlEntrySourceSink : IEntrySourceSink
    {
        static IEnumerable<string> _extensions = new[] { ".html", ".ascx", ".aspx" };
        public IEnumerable<string> Extensions => _extensions;

        public IEnumerable<string> GetValues(FileInfo file)
        {
            var content = File.ReadAllText(file.FullName);
            var parser = new HtmlParser();

            var doc = parser.Parse(content);
            var text = new List<string>();

            GetNodeText(doc.Children, text);

            return text;
        }

        private static void GetNodeText(IHtmlCollection<IElement> elements, List<string> texts)
        {
            foreach (var elem in elements)
            {
                AddToList(elem.TextContent, texts);
                GetSupportedAttributesValues(elem)
                    .ToList()
                    .ForEach(val => AddToList(val, texts));

                if (elem.Children != null && elem.Children.Any())
                    GetNodeText(elem.Children, texts);
            }
        }

        private static void AddToList(string text, List<string> texts)
        {
            var trimmedContent = text.Trim();

            if (!trimmedContent.Contains('\n') && !trimmedContent.Contains('\t') && !string.IsNullOrEmpty(trimmedContent) && !texts.Contains(trimmedContent))
            {
                //switch to interactive
                if(trimmedContent.Contains("<%=") || trimmedContent.Contains("<%#"))
                {
                    if ((trimmedContent.StartsWith("<%=") || trimmedContent.StartsWith("<%#")) && trimmedContent.EndsWith("%>"))
                        return;

                    Console.Clear();
                    Console.WriteLine("Found a WebForms expression:");
                    Console.WriteLine(trimmedContent);
                    Console.WriteLine("What should I replace it with? Just press enter to skip it");
                    trimmedContent = Console.ReadLine();

                    if (string.IsNullOrEmpty(trimmedContent))
                    {
                        Console.WriteLine("Skipping!");
                        return;
                    }
                }

                texts.Add(trimmedContent);
            }
        }

        private static Func<IElement, IEnumerable<string>> GetSupportedAttributesValues
            => elem => new[] {
                    elem.GetAttribute("HeaderText"),
                    elem.GetAttribute("Title"),
                    elem.GetAttribute("InfoText"),
                    elem.GetAttribute("Text"),
                    elem.GetAttribute("title")
                }.Where(x => x != null);
    }
}
