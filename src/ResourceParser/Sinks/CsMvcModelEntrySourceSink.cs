﻿using ResourceParser.Common;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ResourceParser.Sinks
{
    public class CsMvcModelEntrySourceSink : IEntrySourceSink
    {
        static IEnumerable<string> _extensions = new[] { ".cs" };
        static Regex Matcher = new Regex(@"Display\(Name = ""([a-zA-Z ]+)""(, Prompt = ""([a-zA-Z ]+)"")?\)");
        public IEnumerable<string> Extensions => _extensions;

        public IEnumerable<string> GetValues(FileInfo file)
        {
            var content = File.ReadAllText(file.FullName);
            var displayAttributes = Matcher.Matches(content)
               .Select(x => new[] {
                   x.Groups[1].Value,
                   x.Groups[3].Value
               })
               .SelectMany(x => x)
               .Where(x => !string.IsNullOrEmpty(x));

            return displayAttributes;
        }
    }
}
