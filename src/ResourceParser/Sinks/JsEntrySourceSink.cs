﻿using Esprima;
using ResourceParser.Common;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ResourceParser.Sinks
{
    public class JsEntrySourceSink : IEntrySourceSink
    {
        readonly IEnumerable<string> _extensions = new[] { ".ts", ".js" };
        public IEnumerable<string> Extensions => _extensions;

        public IEnumerable<string> GetValues(FileInfo file)
        {
            var filePath = file.FullName.EndsWith(".ts") ? file.FullName.Replace(".ts", ".js") : file.FullName;
            var content = File.ReadAllText(filePath);

            var codeTokens = GetContentTokens(content)
                .Where(x => x.Type == TokenType.StringLiteral)
                .Distinct()
                .Select(x => x.Value.ToString())
                .Where(x => !string.IsNullOrEmpty(x) && !string.IsNullOrWhiteSpace(x) && x.Length > 1 && !x.All(c => !char.IsLetter(c)));

            return codeTokens;
        }

        public IEnumerable<Token> GetContentTokens(string content)
        {
            var tokens = new List<Token>();
            var scanner = new Scanner(content);
            Token token;

            do
            {
                scanner.ScanComments();
                token = scanner.Lex();
                tokens.Add(token);
            } while (token.Type != TokenType.EOF);

            return tokens;
        }
    }
}
