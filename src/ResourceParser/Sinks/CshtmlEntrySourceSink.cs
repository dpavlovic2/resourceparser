﻿using AngleSharp.Dom;
using AngleSharp.Parser.Html;
using ResourceParser.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ResourceParser.Sinks
{
    public class CshtmlEntrySourceSink : IEntrySourceSink
    {
        static IEnumerable<string> _extensions = new[] { ".cshtml" };
        public IEnumerable<string> Extensions => _extensions;

        public IEnumerable<string> GetValues(FileInfo file)
        {
            var content = File.ReadAllText(file.FullName);
            var parser = new HtmlParser();

            var doc = parser.Parse(content);
            var text = new List<string>();

            GetNodeText(doc.Children, text);

            return text;
        }

        private static void GetNodeText(IHtmlCollection<IElement> elements, List<string> texts)
        {
            foreach (var elem in elements)
            {
                AddToList(elem.TextContent, texts);
                GetSupportedAttributesValues(elem)
                    .ToList()
                    .ForEach(val => AddToList(val, texts));

                if (elem.Children != null && elem.Children.Any())
                    GetNodeText(elem.Children, texts);
            }
        }

        private static void AddToList(string text, List<string> texts)
        {
            var trimmedContent = text.Trim();

            //switch to interactive mode
            if (trimmedContent.StartsWith("@") || trimmedContent.Contains("@"))
            {
                if (trimmedContent.Contains('\n') || trimmedContent.Contains('\t'))
                    return;

                Console.Clear();
                Console.WriteLine("Found a Razor expression:");
                Console.WriteLine(trimmedContent);
                Console.WriteLine("What should I replace it with? Just press enter to skip it");
                var replactedString = Console.ReadLine();

                if (string.IsNullOrEmpty(replactedString))
                {
                    Console.WriteLine("Skipping!");
                    return;
                }

                texts.Add(replactedString.Trim());
            }
            else if (!trimmedContent.Contains('\n') && !trimmedContent.Contains('\t') && !string.IsNullOrEmpty(trimmedContent) && !texts.Contains(trimmedContent))
                texts.Add(trimmedContent);
        }

        private static Func<IElement, IEnumerable<string>> GetSupportedAttributesValues
            => elem => new[] {
                    elem.GetAttribute("title"),
                    elem.GetAttribute("value")
                }.Where(x => x != null);
    }
}
