﻿namespace ResourceParser.Common
{
    public static class Constants
    {
        public const string ResEntryFormat = @"/// <summary>
/// '{1}' text.
/// </summary>
public static IResourceEntry {0} {{ get; }} = ResourceEntry.Create(
    typeof({2}), nameof({0}), ""{1}"", ""'{1}' text."");
";
        public const string WebFormsCodeBehind = "public string {0} {{ get; set; }}";
        public const string RazorExpression = "@Html.Localize({0}.{1})";
    }
}
