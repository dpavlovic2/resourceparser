﻿using System.Collections.Generic;
using System.IO;

namespace ResourceParser.Common
{
    public interface IEntrySourceSink
    {
        IEnumerable<string> Extensions { get; }
        IEnumerable<string> GetValues(FileInfo file);
    }
}
